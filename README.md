# AdaptivePy
Master: [![build status](https://gitlab.com/memophysic/adaptive_py_lib/badges/master/build.svg)](https://gitlab.com/memophysic/adaptive_py_lib/commits/master)

Develop: [![build status](https://gitlab.com/memophysic/adaptive_py_lib/badges/develop/build.svg)](https://gitlab.com/memophysic/adaptive_py_lib/commits/develop)

Install with `pip install adaptivepy`

## What is it?
It's a reference library implementing design patterns targeted at easing the 
addition of adaptive behavior to software in an extensible and testable way. 
The project aim at structuring component-based software such that any 
adaptation mechanism can be easily integrated, separately tested and extended
 with no side-effects to the rest of the application.

## Who is it for?
Any Python project which implements adaptivity and wish to be future-proof 
and ensure a certain level of code quality with respect to separation of 
concerns and low-coupling + high-cohesion.

## How to use it?
A paper explaining details of the patterns is under review. Three 
patterns are implemented: 

* Monitor for context sensing and analysis
* Proxy router for architecture restructuration (component substitution)
* Adaptive Component which explicitly declares an adaptation space and uses the two 
 previous ones to allow for extensible construction of arbitrarily large 
 components hierarchies which exhibit adaptation behavior
 
## Can I use it (and for how much)?
The library is FREE software under the terms of the LiLiQ-P license v1.1 
(recognized by the Open Source Initiative).
Details can be found in the LICENSE.txt file.